package com.stecalbert.store.dao;

import com.stecalbert.store.model.Cart;
import com.stecalbert.store.model.ProductInCart;

import java.util.Optional;

public interface CartDao {

    Optional<Cart> findByUsername(String username);

    boolean createOrUpdateCart(Cart cart);

    boolean deleteCartProduct(ProductInCart product, Cart cart);
}
