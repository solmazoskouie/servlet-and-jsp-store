package com.stecalbert.store.dao;

import com.stecalbert.store.model.User;

import java.util.List;
import java.util.Optional;

public interface UserDao {

    List<User> findAll();

    Optional<User> findByUsername(String username);

    boolean create(User user);

    boolean update(User user);
}
