package com.stecalbert.store.dao.db_impl;

import com.stecalbert.store.dao.CartDao;
import com.stecalbert.store.model.Cart;
import com.stecalbert.store.model.ProductInCart;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CartDaoDbImpl implements CartDao {
    private static final Logger LOGGER = Logger.getLogger(CartDaoDbImpl.class.getName());
    private static final String PERSISTENCE_UNIT_NAME = "store";
    private static final EntityManagerFactory factory =
            Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    @Override
    public Optional<Cart> findByUsername(String username) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Optional user = em.createQuery("from carts WHERE username=?1")
                .setParameter(1, username)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst();
        em.getTransaction().commit();
        em.close();
        LOGGER.log(Level.INFO, "Fetched user from database: {0}" + user);
        return user;
    }

    @Override
    public boolean createOrUpdateCart(Cart cart) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(cart);
        em.getTransaction().commit();
        em.close();
        LOGGER.log(Level.INFO, "Merged cart : {0}" + cart);
        return true;
    }

    @Override
    public boolean deleteCartProduct(ProductInCart product, Cart cart) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        // todo : Potrzebna poprawka, usuwać wszystkie z danej karty, a nie z całej tabeli
        Query q = em.createQuery("delete products_in_cart where id = ?1")
                .setParameter(1, product.getId());
        int result = q.executeUpdate(); // zwraca ilość usuniętych wierszy, jeśli 0 nic nie usunął
        em.getTransaction().commit();
        LOGGER.log(Level.INFO, "ProductInCart {0} deleted:", product);
        return result == 1;
    }

}
