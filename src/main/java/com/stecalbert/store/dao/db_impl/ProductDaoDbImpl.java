package com.stecalbert.store.dao.db_impl;

import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.model.StoreProduct;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductDaoDbImpl implements ProductDao {
    private static final Logger LOGGER = Logger.getLogger(ProductDaoDbImpl.class.getName());
    private static final String PERSISTENCE_UNIT_NAME = "store";
    private static final EntityManagerFactory factory =
            Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    @Override
    public List<StoreProduct> findAll() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query queryResult = em.createQuery("from products");
        List<StoreProduct> storeProducts = queryResult.getResultList();
        em.getTransaction().commit();
        em.close();
        LOGGER.log(Level.INFO, "Fetched all storeProducts from database: {0)", storeProducts);
        return storeProducts;
    }

    @Override
    public StoreProduct findById(Long id) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        StoreProduct storeProduct = em.find(StoreProduct.class, id);
        em.getTransaction().commit();
        LOGGER.log(Level.INFO, "StoreProduct {0} fetched by id", storeProduct);
        return storeProduct;
    }

    @Override
    public boolean update(StoreProduct storeProduct) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(storeProduct);
        em.getTransaction().commit();
        LOGGER.log(Level.INFO, "StoreProduct {0} fetched by id", storeProduct);
        return true;
    }


    @Override
    public boolean delete(StoreProduct product) {
        Long id = product.getId();
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.remove(em.contains(product) ? product : em.merge(product));

        // Usunięcie wszystkich zależnych rekordów w tabeli products_in_cart
        Query q = em.createQuery("delete products_in_cart where product_id = ?1").setParameter(1, id);
        q.executeUpdate();

        em.getTransaction().commit();
        LOGGER.log(Level.INFO, "StoreProduct {0} deleted:", product);
        return true;
    }

    @Override
    public StoreProduct insert(StoreProduct storeProduct) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.persist(storeProduct);
        em.getTransaction().commit();
        LOGGER.log(Level.INFO, "StoreProduct {0} created", storeProduct);
        return storeProduct;
    }
}
