package com.stecalbert.store.dao.db_impl;

import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDaoDbImpl implements UserDao {
    private static final Logger LOGGER = Logger.getLogger(UserDaoDbImpl.class.getName());
    private static final String PERSISTENCE_UNIT_NAME = "store";
    private static final EntityManagerFactory factory =
            Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);

    @Override
    public List<User> findAll() {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Query queryResult = em.createQuery("from users");
        List<User> allUsers = queryResult.getResultList();
        em.getTransaction().commit();
        em.close();
        LOGGER.log(Level.INFO, "Fetched all users from database: {0)", allUsers);
        return allUsers;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        Optional user = em.createQuery("from users WHERE username=?1")
                .setParameter(1, username)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst();
        em.getTransaction().commit();
        em.close();
        LOGGER.log(Level.INFO, "Fetched user from database: {0}" + user);
        return user;
    }

    @Override
    public boolean create(User user) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        em.close();
        LOGGER.log(Level.INFO, "Persisted user in database: {0}" + user);
        return true;
    }

    @Override
    public boolean update(User user) {
        EntityManager em = factory.createEntityManager();
        em.getTransaction().begin();
        em.merge(user);
        em.getTransaction().commit();
        em.close();
        LOGGER.log(Level.INFO, "Updated user in database: {0}" + user);
        return true;
    }

}
