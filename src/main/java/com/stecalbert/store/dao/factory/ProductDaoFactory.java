package com.stecalbert.store.dao.factory;

import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.dao.db_impl.ProductDaoDbImpl;
import com.stecalbert.store.dao.json_impl.ProductDaoJsonImpl;
import com.stecalbert.store.settings.DaoType;

public class ProductDaoFactory {

    public static ProductDao getProductDao(DaoType daoType) {
        switch (daoType) {
            case JSON:
                return new ProductDaoJsonImpl();
            case MY_SQL:
                return new ProductDaoDbImpl();
            default:
                return new ProductDaoJsonImpl();
        }
    }
}
