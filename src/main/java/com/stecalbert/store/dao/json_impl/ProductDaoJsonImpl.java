package com.stecalbert.store.dao.json_impl;

import com.stecalbert.store.dao.JsonDao;
import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.model.JsonAccess;
import com.stecalbert.store.model.StoreProduct;

import java.util.List;
import java.util.OptionalLong;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ProductDaoJsonImpl implements ProductDao, JsonDao {
    private static final Logger LOGGER = Logger.getLogger(ProductDaoJsonImpl.class.getName());

    @Override
    public List<StoreProduct> findAll() {
        List<StoreProduct> products = getJson().getProducts();
        LOGGER.log(Level.INFO, "Fetched all storeProducts from database: {0)", products);
        return products;
    }

    // todo: dodac Optional
    @Override
    public StoreProduct findById(Long id) {
        List<StoreProduct> products = getJson().getProducts();
        StoreProduct storeProduct = products.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .get();
        LOGGER.log(Level.INFO, "StoreProduct {0} fetched by id", storeProduct);
        return storeProduct;
    }

    @Override
    public boolean update(StoreProduct storeProduct) {
        JsonAccess jsonAccess = getJson();
        jsonAccess.getProducts().removeIf(e -> e.getId().equals(storeProduct.getId()));
        jsonAccess.getProducts().add(storeProduct);
        writeJson(jsonAccess);
        LOGGER.log(Level.INFO, "StoreProduct {0} fetched by id", storeProduct);
        return true;
    }

    @Override
    public boolean delete(StoreProduct product) {
        JsonAccess jsonAccess = getJson();
        jsonAccess.getProducts().removeIf(e -> e.getId().equals(product.getId()));

        // Usunięcie wszystkich zależnych rekordów w tablicy ShoppinCarts
        jsonAccess.getShoppingCarts().forEach(e -> {
            e.getProducts().removeIf(p -> p.getProductId().equals(product.getId()));
        });

        writeJson(jsonAccess);
        LOGGER.log(Level.INFO, "StoreProduct {0} deleted:", product);
        return true;
    }

    @Override
    public StoreProduct insert(StoreProduct storeProduct) {
        JsonAccess jsonAccess = getJson();
        Long id = getNextId(jsonAccess.getProducts());
        storeProduct.setId(id);
        jsonAccess.getProducts().add(storeProduct);
        writeJson(jsonAccess);
        LOGGER.log(Level.INFO, "StoreProduct {0} created", storeProduct);
        return storeProduct;
    }

    private Long getNextId(List<StoreProduct> storeProducts) {
        OptionalLong optionalId = storeProducts.stream()
                .mapToLong(StoreProduct::getId)
                .max();
        Long id = optionalId.orElse(0L);
        return id + 1;
    }
}
