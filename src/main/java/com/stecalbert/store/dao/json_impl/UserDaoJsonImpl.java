package com.stecalbert.store.dao.json_impl;

import com.stecalbert.store.dao.JsonDao;
import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.model.JsonAccess;
import com.stecalbert.store.model.User;

import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UserDaoJsonImpl implements UserDao, JsonDao {
    private static final Logger LOGGER = Logger.getLogger(UserDaoJsonImpl.class.getName());

    @Override
    public List<User> findAll() {
        List<User> users = getJson().getUsers();
        LOGGER.log(Level.INFO, "Fetched all users from database: {0)", users);
        return users;
    }

    @Override
    public Optional<User> findByUsername(String username) {
        List<User> users = getJson().getUsers();
        Optional<User> user = users.stream()
                .filter(u -> u.getUsername().equals(username))
                .findFirst();
        LOGGER.log(Level.INFO, "Fetched user from database: {0}" + user);
        return user;
    }

    @Override
    public boolean create(User user) {
        JsonAccess jsonAccess = getJson();
        getJson().getUsers().add(user);
        writeJson(jsonAccess);
        LOGGER.log(Level.INFO, "Persisted user in database: {0}" + user);
        return true;
    }

    @Override
    public boolean update(User user) {
        JsonAccess jsonAccess = getJson();
        User oldUser = findById(jsonAccess.getUsers(), user.getId());
        jsonAccess.getUsers().remove(oldUser);
        jsonAccess.getUsers().add(user);
        writeJson(jsonAccess);
        LOGGER.log(Level.INFO, "Updated user in database: {0}" + user);
        return true;
    }

    private User findById(List<User> users, Long id) {
        Optional<User> optionalUserToUpdate = users.stream()
                .filter(u -> u.getId().equals(id))
                .findFirst();
        return optionalUserToUpdate.orElseThrow(() -> new RuntimeException("Couldn't find user"));
    }

}
