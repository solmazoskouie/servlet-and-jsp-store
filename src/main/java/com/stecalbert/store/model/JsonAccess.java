package com.stecalbert.store.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.List;

@Data
public class JsonAccess {

    @SerializedName("users")
    @Expose
    private List<User> users = null;
    @SerializedName("products")
    @Expose
    private List<StoreProduct> products = null;
    @SerializedName("shoppingCarts")
    @Expose
    private List<Cart> shoppingCarts = null;
}