package com.stecalbert.store.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.persistence.*;

@Entity(name = "products_in_cart")
public class ProductInCart {

    @Id
    @GeneratedValue
    @Column(name = "id")
    @Expose
    private Long id;

    @Column(name = "product_id")
    @Expose
    @SerializedName("product_id")
    private Long productId;

    @Column(name = "quantity")
    @Expose
    private int quantity;

    @ManyToOne
    @JoinColumn(name = "cart_id", nullable = false)
    private Cart cart;

    public ProductInCart() {
    }

    public ProductInCart(Long productId, int quantity, Cart cart) {
        this.productId = productId;
        this.quantity = quantity;
        this.cart = cart;
    }

    public ProductInCart(Long productId, int quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }

    public void incrementQuantity() {
        this.quantity += 1;
    }

    public void decrementQuantity() {
        this.quantity -= 1;
    }

    public long getId() {
        return id;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }


}
