package com.stecalbert.store.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "products")
public class StoreProduct {

    @SerializedName("id")
    @Expose
    @GeneratedValue
    @Id
    private Long id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("description")
    @Expose
    private String description;

    //todo : zamienić na BigDecimal
    @SerializedName("price")
    @Expose
    private Integer price;

    @SerializedName("quantity")
    @Expose
    private Integer quantity;

    public StoreProduct(String name, String description, Integer price, Integer quantity) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
    }

    public StoreProduct(Long id, String name, String description, Integer price, Integer quantity) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
    }

    public StoreProduct() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Integer getPrice() {
        return price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void decrementQuantity() {
        this.quantity -= 1;
    }

    public void incrementQuantity() {
        this.quantity += 1;
    }

    public void setId(Long id) {
        this.id = id;
    }
}