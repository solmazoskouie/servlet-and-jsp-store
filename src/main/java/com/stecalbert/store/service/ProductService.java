package com.stecalbert.store.service;

import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.model.StoreProduct;

import java.util.List;

public class ProductService {
    private ProductDao productDao;

    public ProductService(ProductDao productDao) {
        this.productDao = productDao;
    }

    public List<StoreProduct> getAll() {
        return productDao.findAll();
    }

    public StoreProduct getById(Long productId) {
        return productDao.findById(productId);
    }

    public boolean update(StoreProduct storeProduct) {
        return productDao.update(storeProduct);
    }

    public boolean delete(StoreProduct storeProduct) {
        return productDao.delete(storeProduct);
    }

    public StoreProduct insert(StoreProduct storeProduct) {
        return productDao.insert(storeProduct);
    }

}
