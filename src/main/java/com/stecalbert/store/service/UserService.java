package com.stecalbert.store.service;

import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.model.User;

import javax.servlet.http.Cookie;
import java.util.Arrays;
import java.util.Optional;

public class UserService {
    private UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public Optional<User> getByUsername(String username) {
        return userDao.findByUsername(username);
    }

    public boolean validateUser(String username, String password) {
        boolean isValid = false;
        Optional<User> user = userDao.findByUsername(username);
        if (user.isPresent()) {
            isValid = user.get().getPassword().equals(password);
        }
        return isValid;
    }

    public Cookie createCookie(String username) {
        Cookie cookie = new Cookie("user", username);
        cookie.setMaxAge(30 * 60);
        return cookie;
    }

    public boolean register(User user) {
        return userDao.create(user);
    }

    public static Optional<Cookie> getUserCookie(Cookie[] cookies) {
        Optional<Cookie> cookie = Optional.empty();
        if (cookies != null) {
            cookie = Arrays.stream(cookies)
                    .filter(c -> c.getName().equals("user"))
                    .findFirst();
        }
        return cookie;
    }

    public boolean update(User user) {
        return userDao.update(user);
    }
}
