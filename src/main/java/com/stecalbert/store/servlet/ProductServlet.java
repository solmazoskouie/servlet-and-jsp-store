
package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.dao.factory.ProductDaoFactory;
import com.stecalbert.store.dao.factory.UserDaoFactory;
import com.stecalbert.store.model.StoreProduct;
import com.stecalbert.store.model.User;
import com.stecalbert.store.service.ProductService;
import com.stecalbert.store.service.UserService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@WebServlet("/Products")
public class ProductServlet extends HttpServlet {
    private ProductService productService;
    private UserService userService;

    @Override
    public void init() throws ServletException {
        UserDao userDao = UserDaoFactory.getUserDao(Settings.DAO_TYPE);
        userService = new UserService(userDao);
        ProductDao productDao = ProductDaoFactory.getProductDao(Settings.DAO_TYPE);
        productService = new ProductService(productDao);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = UserService.getUserCookie(req.getCookies()).get().getValue(); // zalogowany użytkownik musi posiadać ciasteczko
        Optional<User> optionalUserser = userService.getByUsername(username);
        User user = optionalUserser.orElseThrow(() -> new RuntimeException("User does not exist in db"));
        req.setAttribute("role", user.getRole());
        List<StoreProduct> productList = productService.getAll();
        productList.sort(Comparator.comparing(StoreProduct::getId));
        req.setAttribute("storeProducts", productList);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("products.jsp");
        requestDispatcher.forward(req, resp);
    }
}
