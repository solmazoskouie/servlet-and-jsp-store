package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.UserDao;
import com.stecalbert.store.dao.factory.UserDaoFactory;
import com.stecalbert.store.model.User;
import com.stecalbert.store.service.UserService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.math.BigDecimal;

@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
    private UserService userService;

    @Override
    public void init() throws ServletException {
        UserDao userDao = UserDaoFactory.getUserDao(Settings.DAO_TYPE);
        userService = new UserService(userDao);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        String surname = req.getParameter("surname");
        User user = new User(username, password, new BigDecimal(0),
                name, surname, "user");

        boolean wasCreated = userService.register(user);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("index.jsp");
        if (wasCreated) {
            requestDispatcher.forward(req, resp);
        } else {
            resp.getWriter().println("<p>An error occured, cannot register user/p>");
            requestDispatcher.include(req, resp);
        }

    }
}
