package com.stecalbert.store.servlet;

import com.stecalbert.store.dao.CartDao;
import com.stecalbert.store.dao.ProductDao;
import com.stecalbert.store.dao.factory.CartDaoFactory;
import com.stecalbert.store.dao.factory.ProductDaoFactory;
import com.stecalbert.store.service.CartService;
import com.stecalbert.store.service.UserService;
import com.stecalbert.store.settings.Settings;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/RemoveCartProduct")
public class RemoveProductFromCartServlet extends HttpServlet {
    private CartService cartService;

    @Override
    public void init() throws ServletException {
        CartDao cartDao = CartDaoFactory.getCartDao(Settings.DAO_TYPE);
        ProductDao productDao = ProductDaoFactory.getProductDao(Settings.DAO_TYPE);
        cartService = new CartService(cartDao, productDao);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String productId = req.getParameter("productId");
        Optional<Cookie> usernameCookie = UserService.getUserCookie(req.getCookies()); // zalogowany użytkownik musi posiadać ciasteczko
        usernameCookie.ifPresent(e ->
                cartService.removeFromCart(Long.valueOf(productId), usernameCookie.get().getValue()));
        resp.sendRedirect("Cart");
    }
}
