package com.stecalbert.store.settings;

public enum DaoType {
    JSON,
    MY_SQL
}
