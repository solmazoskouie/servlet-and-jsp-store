<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%--Skrypt blokujący przycisk wstecz przeglądarki--%>
    <script>
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, document.URL);
        });
    </script>
</head>
<body>
<h1>Your cart</h1>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price (PLN)</th>
        <th>Quantity in cart</th>
        <th></th>
    </tr>
    <c:forEach var="storeProduct" items="${productsInCart}">
        <form action="RemoveCartProduct" method="post">
            <tr>
                <td><input style="width: 20px" name="productId" value="${storeProduct.key.getId()}" readonly/></td>
                <td>${storeProduct.key.getName()}</td>
                <td>${storeProduct.key.getDescription()}</td>
                <td>${storeProduct.key.getPrice()}</td>
                <td>${storeProduct.value}</td>
                <td>
                    <button type="submit" value="remove">Remove from cart</button>
                </td>
            </tr>
        </form>
    </c:forEach>
</table>

<a href="/Products">Back to store</a><br><br>

<a href="/Checkout">Checkout</a>

</body>
</html>
