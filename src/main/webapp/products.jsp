<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <%--Skrypt blokujący przycisk wstecz przeglądarki--%>
    <script>
        history.pushState(null, null, document.URL);
        window.addEventListener('popstate', function () {
            history.pushState(null, null, document.URL);
        });
    </script>
</head>
<body>
<h1>Products store</h1>
<table border="1">
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Description</th>
        <th>Price (PLN)</th>
        <th>Quantity</th>
    </tr>
    <c:forEach var="storeProduct" items="${storeProducts}">
        <form method="post">
            <tr>
                <td><input style="width: 20px" id="productId" name="productId" value="${storeProduct.getId()}"
                           readonly/></td>
                <td>${storeProduct.getName()}</td>
                <td>${storeProduct.getDescription()}</td>
                <td>${storeProduct.getPrice()}</td>
                <td>${storeProduct.getQuantity()}</td>
                <td>
                    <c:if test="${storeProduct.getQuantity() > 0}">
                        <input type="submit" value="Add to cart" formaction="AddCartProduct"/>
                    </c:if>
                </td>
                <c:if test="${role == 'admin'}">
                    <td>
                        <button type="submit" value="Edit" formaction="showEdit">Edit</button>
                    </td>
                    <td>
                        <button type="submit" value="Remove" formaction="removeStoreProduct">Remove</button>
                    </td>
                </c:if>
            </tr>
        </form>
    </c:forEach>
</table>
<a href="/Cart">Your cart</a><br><br>
<a href="/Logout">Logout</a><br><br>
<c:if test="${role == 'admin'}">
    <a href="/showAddForm">Add product</a>
</c:if>
</body>
</html>
